import { useEffect, useState } from "react";

const SimpleInput = (props) => {
    const [name, setName] = useState("");
    const [nameTouched, setNameTouched] = useState(false);
    const [isNameValid, setIsNameValid] = useState(true);

    const [email, setEmail] = useState("");
    const [emailTouched, setEmailTouched] = useState(false);
    const [emailBlured, setEmailBlured] = useState(false);
    const [isEmailValid, setIsEmailValid] = useState(true);

    const [isFormValid, setIsFormValid] = useState(false);

    useEffect(() => {
        setIsNameValid(name.trim() !== "" || (name.trim() === "" && !nameTouched));
        setIsEmailValid(emailIsValid());
        
        function emailIsValid() {
            return (
              (emailBlured && !email.match("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:.[a-zA-Z0-9-]+)*$")) ||
                (email.trim() === "" && !emailTouched)
            );
        }
    }, [name, nameTouched, isNameValid, email, isEmailValid, emailTouched, emailBlured]);

    useEffect(() => {
      setIsFormValid(isNameValid && nameTouched && isEmailValid && emailTouched);
    }, [isNameValid, nameTouched, isEmailValid, emailTouched]);

    function nameChangeHandler(event) {
        event.preventDefault();
        setName(event.target.value);
    }

    function nameBlurHandler(event) {
        event.preventDefault();
        if (!nameTouched) {
            setNameTouched(true);
        }
    }

    function emailChangeHandler(event) {
        event.preventDefault();
        setEmail(event.target.value);
    }

    function emailBlurHandler(event) {
        event.preventDefault();
        if (!nameTouched) {
            setEmailTouched(true);
        }
        if (!emailBlured) {
            setEmailBlured(true);
        }
    }

    function submitHandler(event) {
        event.preventDefault();
        console.log("submitHandler");
    }

    return (
        <form onSubmit={submitHandler}>
            <div className={isNameValid ? "form-control" : "form-control invalid"}>
                <label htmlFor='name'>Your Name</label>
                <input value={name} type='text' id='name' onChange={nameChangeHandler} onBlur={nameBlurHandler} />
            </div>
            <div className={isEmailValid ? "form-control" : "form-control invalid"}>
                <label htmlFor='email'>Your Email</label>
                <input value={email} type='text' id='email' onChange={emailChangeHandler} onBlur={emailBlurHandler} />
            </div>
            <div className='form-actions'>
                <button disabled={!isFormValid}>Submit</button>
            </div>
        </form>
    );
};

export default SimpleInput;
