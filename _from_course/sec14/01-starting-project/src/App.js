import React, { useEffect, useState } from "react";

import MoviesList from "./components/MoviesList";
import "./App.css";

function App() {
    const [movies, setMovies] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState(null);

    useEffect(() => {
      // in loading time
      fetchMoviesHandlerAwait();
    }, []);

    function fetchMoviesHandler() {
        setIsLoading(true);
        setError(null);
        fetch("https://swapi.dev/api/films")
            .then((response) => {
                if (!response.ok) {
                    throw new Error("Something went wrong!");
                }

                return response.json();
            })
            .then((data) => {
                setMovies(data.results);
            })
            .catch((error) => {
                setError(error.message);
            });
        setIsLoading(false);
    }

    async function fetchMoviesHandlerAwait() {
        setIsLoading(true);
        setError(null);
        try {
            const response = await fetch("https://swapi.dev/api/films");
            if (!response.ok) {
                throw new Error("Something went wrong!");
            }

            const data = await response.json();
            setMovies(data.results);
        } catch (error) {
            setError(error.message);
        }
        setIsLoading(false);
    }

    return (
        <React.Fragment>
            <section>
                <button onClick={fetchMoviesHandlerAwait}>Fetch Movies</button>
            </section>
            <section>
                {error === null && isLoading && <p>Loading ...</p>}
                {error === null && !isLoading && movies.length === 0 && <p>Movies list is empty!!!</p>}
                {error === null && !isLoading && movies.length > 0 && <MoviesList movies={movies} />}
                {error !== null && <p>{error}</p>}
            </section>
        </React.Fragment>
    );
}

export default App;
