import jwtDecode, { JwtPayload } from 'jwt-decode';

export type TOKEN_TYPE = 'token' | 'refreshToken';

export interface CustomJwtPayload extends JwtPayload {
    employeeId: number;
    roles?: string[];
    refresh?: boolean;
}

const storedAuth: {
    token: string | null;
    refreshToken: string | null;
    remember: boolean; // should remember in localStorage to preserve while reload page
} = {
    token: null,
    refreshToken: localStorage.getItem('refreshToken'),
    remember: localStorage.getItem('refreshToken') !== null, // if exists than it should remember after refresh too
};

const isTokenValid = (exp: number | undefined): boolean => {
    // Token valid for less then 1 minutes is conssidered expired
    return (exp ?? 0) - 60 > Date.now() / 1000;
};

const decodeToken = (token: string | null): CustomJwtPayload | null => {
    if (token) {
        const decoded = jwtDecode<CustomJwtPayload>(token);
        if (isTokenValid(decoded.exp)) {
            return decoded;
        }
    }
    return null;
};

export const getDecodedToken = (type: TOKEN_TYPE) => {
    const token = decodeToken(getToken(type));
    if (!token) {
        removeToken(type);
    }
    return token;
};

export const getToken = (type: TOKEN_TYPE) => {
    const token = storedAuth[type];
    if (token) {
        return token;
    } else {
        return localStorage.getItem(type);
    }
};

export const removeToken = (type: TOKEN_TYPE) => {
    storedAuth[type] = null;
    if (type) {
        storedAuth.remember = false;
        localStorage.removeItem(type);
    }
};

export const storeToken = (
    type: TOKEN_TYPE,
    token: string,
    remember?: boolean
) => {
    storedAuth[type] = token;
    if (remember !== undefined) {
        storedAuth.remember = remember;
    } else {
        remember = storedAuth.remember; // if not set, than use already stored value
    }
    if (remember) {
        localStorage.setItem(type, token);
    }
};