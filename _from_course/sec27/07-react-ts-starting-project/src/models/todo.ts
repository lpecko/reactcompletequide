class Todo {
    id: number;
    text: string;

    constructor(todoText: string) {
        this.id = Date.now();
        this.text = todoText;
    }
}

export default Todo;
