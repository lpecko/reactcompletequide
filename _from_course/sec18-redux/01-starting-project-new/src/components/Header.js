import classes from './Header.module.css';
import { useDispatch, useSelector } from 'react-redux';
import { authActions } from '../store/slice/authSlice';

const Header = () => {

  const authenticated = useSelector(state => state.auth.isAuthenticated)
  const dispatch = useDispatch();

  return (
    <header className={classes.header}>
      <h1>Redux Auth</h1>
      {authenticated && <nav>
        <ul>
          <li>
            <a href='/'>My Products</a>
          </li>
          <li>
            <a href='/'>My Sales</a>
          </li>
          <li>
            <button onClick={() => { dispatch(authActions.setAuthenticated(false)) }}>Logout</button>
          </li>
        </ul>
      </nav>}
    </header>
  );
};

export default Header;
