import React, { useRef } from "react";
import Todo from "../models/todo";
import classes from "./NewTodo.module.css";

// const NewTodo: React.FC<{ addTodo: Function }> = (props) => {
const NewTodo: React.FC<{ addTodo: (todo: Todo) => void }> = (props) => {
    const textRef = useRef<HTMLInputElement>(null);

    const submitHandler = (event: React.FormEvent) => {
        event.preventDefault();
        const enteredText = textRef.current!.value;
        props.addTodo(new Todo(enteredText));
    };

    return (
        <form onSubmit={submitHandler} className={classes.form}>
            <div>
                <label htmlFor='todoText'>TODO text</label>
                <input id='todoText' type='text' ref={textRef} />
            </div>
            <button type='submit'>Add</button>
        </form>
    );
};

export default NewTodo;
