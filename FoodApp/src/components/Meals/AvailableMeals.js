import MealItem from "./MealItem";
import Card from "../UI/Card";
import classes from "./AvailableMeals.module.css";
import { useContext, useEffect, useState } from "react";
import ApplicationContext from "../../context/ApplicationContext";

function AvailableMeals(props) {
    const [meals, setMeals] = useState([]);
    const appCtx = useContext(ApplicationContext);
    const setLoading = appCtx.setIsLoading;

    useEffect(() => {
        setLoading(true);
        fetch("https://react-http-f4814-default-rtdb.europe-west1.firebasedatabase.app/meals.json")
            .then((resp) => {
                return resp.json();
            })
            .then((data) => {
                let fetchedMeals = [];

                for (const key in data) {
                    fetchedMeals.push({
                        id: key,
                        name: data[key].name,
                        description: data[key].description,
                        price: data[key].price,
                    });
                }
                setMeals(fetchedMeals);
            });
        setLoading(false);
    }, [setLoading]);

    return (
        <Card className={classes.meals}>
            <ul>
                {meals.map((element) => (
                    <MealItem
                        key={element.id}
                        id={element.id}
                        name={element.name}
                        description={element.description}
                        price={element.price}
                    />
                ))}
            </ul>
        </Card>
    );
}

export default AvailableMeals;
