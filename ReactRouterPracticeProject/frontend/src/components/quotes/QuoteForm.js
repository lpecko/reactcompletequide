import { Fragment, useRef } from "react";
import { useNavigate } from "react-router-dom";
import { saveQuotes } from "../../api/QuoteApi";

import Card from "../ui/Card";
import LoadingSpinner from "../ui/LoadingSpinner";
import classes from "./QuoteForm.module.css";

const QuoteForm = (props) => {
    const navigate = useNavigate();
    const authorInputRef = useRef();
    const textInputRef = useRef();
    // const [isEntering, setIsEntering] = useState(false);

    function submitFormHandler(event) {
        event.preventDefault();

        const enteredAuthor = authorInputRef.current.value;
        const enteredText = textInputRef.current.value;

        saveQuotes({
            id: Math.random(),
            author: enteredAuthor,
            text: enteredText,
        });

        navigate("../quotes");

        // optional: Could validate here
        //props.onAddQuote({ author: enteredAuthor, text: enteredText });
    }

    // const formFocusedHandler = () => {
    //     setIsEntering(true);
    // };

    // const formFinishedHandler = () => {
    //     setIsEntering(false);
    // };

    return (
        <Fragment>
            {/* <Prompt
                when={isEntering}
                message={(location) => {
                    console.log(location);
                    return `Are you sure to want a leave form in ${location.pathname}?`;
                }}
            />  zaniklo po vidani verzie 6*/}
            <Card>
                <form className={classes.form} onSubmit={submitFormHandler}>
                    {props.isLoading && (
                        <div className={classes.loading}>
                            <LoadingSpinner />
                        </div>
                    )}

                    <div className={classes.control}>
                        <label htmlFor='author'>Author</label>
                        <input type='text' id='author' ref={authorInputRef} />
                    </div>
                    <div className={classes.control}>
                        <label htmlFor='text'>Text</label>
                        <textarea id='text' rows='5' ref={textInputRef}></textarea>
                    </div>
                    <div className={classes.actions}>
                        <button className='btn'>Add Quote</button>
                    </div>
                </form>
            </Card>
        </Fragment>
    );
};

export default QuoteForm;
