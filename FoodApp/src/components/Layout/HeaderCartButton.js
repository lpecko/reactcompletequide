import { useContext, useEffect, useState } from "react";
import CartContext from "../../context/CartContext";
import CartIcon from "./CartIcon";
import classes from "./HeaderCartButton.module.css";

function HeaderCartButton(props) {
    const cartCtx = useContext(CartContext);
    const [mealsCartSize, setMealsCartSize] = useState(0);

    function openCartHandler(event) {
        event.preventDefault();
        cartCtx.setShowedCart(true);
    }

    useEffect(() => {
        setMealsCartSize(cartCtx.mealsCart.map((m) => Number(m.amount)).reduce((a, b) => a + b, 0));
    }, [cartCtx.mealsCart]);

    return (
        <button className={classes.button} onClick={openCartHandler}>
            <span className={classes.icon}>
                <CartIcon />
            </span>
            <span>Your Cart</span>
            <span className={classes.badge}>{mealsCartSize}</span>
        </button>
    );
}

export default HeaderCartButton;
