import NewEmployee from "../employee/NewEmployee";
import classes from "./Body.module.css";

const Body = () => {
    return (
        <div className={classes.body}>
            <div className={classes.board}>
                <NewEmployee />
            </div>
        </div>
    );
};

export default Body;
