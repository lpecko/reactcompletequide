// import { createStore } from "redux";
import { configureStore } from "@reduxjs/toolkit";
import authSlice from "./slice/authSlice";
import counterSlice from "./slice/counterSlice";


const store = configureStore({
    reducer: {counter: counterSlice.reducer, auth: authSlice.reducer}
});

export default store;
