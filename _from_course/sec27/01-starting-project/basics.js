"use strict";
exports.__esModule = true;
//primitive types
var name;
name = "Elen";
var age;
age = 28;
var isWoman;
isWoman = false;
// isWoman = "adfs";
//arrays
var names;
names = ["Elen", "Peter"];
// one row declaration
var ordName = "Peter"; //declared with first initialization type (type inferrence)
var ordNameRedundant = "Peter"; // redundant type declaration
// let ordNameWrong: string = 15;
// objects
var person;
person = {
    name: "Elen",
    age: 28,
    isWoman: false
};
// object arrays
var persons;
persons = [
    { name: "Elen", age: 28, isWoman: false },
    { name: "Peter", age: 24, isWoman: false },
];
// union types
var anyType; // default type, like javascript
var numStringType;
anyType = 12;
anyType = "Paul";
anyType = true;
numStringType = 12;
numStringType = "Paul";
var personTyped;
var personArrayTyped;
// function & types
function add(a, b) {
    return a + b;
}
function addRedundant(a, b) {
    // return type definition is redundant
    return a + b;
}
var remove = function (a, b) {
    return a - b;
};
