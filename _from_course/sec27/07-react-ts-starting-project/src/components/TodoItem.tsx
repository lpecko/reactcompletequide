import React from "react";
import Todo from "../models/todo";
import classes from "./TodoItem.module.css";

const TodoItem: React.FC<{ item: Todo; remove: (id: number) => void }> = (props) => (
    <li
        className={classes.item}
        onClick={() => {
            props.remove(props.item.id);
        }}
    >
        {props.item.text}
    </li>
);
export default TodoItem;
