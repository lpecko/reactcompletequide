import React, { useState } from 'react';

const ApplicationContext = React.createContext({
    isLoading: false,
    setIsLoading: () => {},
});

export default ApplicationContext;

export function ApplicationContextProvider(props) {

    const[isLoading, setIsLoading] = useState(false);

    return (
        <ApplicationContext.Provider value={{
            isLoading: isLoading,
            setIsLoading: setIsLoading
        }}>
            {props.children}
        </ApplicationContext.Provider>
    );

}