import React from "react";
import Todo from "../models/todo";
import TodoItem from "./TodoItem";
import classes from "./Todos.module.css";

const Todos: React.FC<{ items: Todo[]; remove: (id: number) => void }> = (props) => {
    return (
        <ul className={classes.todos}>
            {props.items.map((i) => (
                <TodoItem key={i.id} item={i} remove={props.remove} />
            ))}
        </ul>
    );
};

export default Todos;
