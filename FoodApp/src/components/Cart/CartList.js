import classes from "./Cart.module.css";
import CartItem from "./CartItem";

function CartList(props) {
    return (
        <div className={classes["cart-items"]}>
            {props.mealsCart.length === 0 && <div>I'am so empty.</div>}
            {props.mealsCart.length > 0 &&
                props.mealsCart.map((item) => (
                    <CartItem key={item.id} id={item.id} name={item.name} price={item.price} amount={item.amount} />
                ))}
        </div>
    );
}

export default CartList;