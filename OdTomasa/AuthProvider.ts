import React, { useCallback, useEffect, useState } from 'react';

import { Api } from '../openapi';
import {
    getDecodedToken,
    getToken,
    removeToken,
    storeToken,
} from '../utils/token';
import Loader from '../components/UI/Loader';
import { useApi } from '../hooks/useApi';
import { processError } from '../utils/apiError';
import { useSnackbar } from 'notistack';
import { useTranslation } from 'react-i18next';

type AuthProviderProps = {
    children?: React.ReactNode;
};

type AuthUser = {
    employeeId: number;
    roles: string[];
};

type AuthContextValue = {
    user: AuthUser | undefined;
    logIn: (credentials: LoginParams) => Promise<void>;
    logOut: () => void;
};

export interface LoginParams extends Api.LoginRequest {
    remember: boolean;
}

export const AuthContext = React.createContext<AuthContextValue>(
    {} as AuthContextValue
);

const getAuthUserFromStoredToken = (): AuthUser | undefined => {
    const token = getDecodedToken('token');
    if (token) {
        return {
            employeeId: token.employeeId,
            roles: token.roles ?? [],
        };
    }
    return undefined;
};

const AuthProvider: React.FC<AuthProviderProps> = ({ children }) => {
    const { authApi } = useApi();

    const [user, setUser] = useState<AuthUser | undefined>(
        getAuthUserFromStoredToken()
    );
    const [loading, setLoading] = useState(true);
    const { enqueueSnackbar } = useSnackbar();
    const { t } = useTranslation();

    const removeStoredTokens = useCallback(() => {
        removeToken('token');
        removeToken('refreshToken');
        setUser(undefined);
    }, []);

    const updateStoredTokens = useCallback(
        (response: Api.TokenResponse | undefined, remember?: boolean) => {
            if (response) {
                storeToken('token', response.token, remember);
                storeToken('refreshToken', response.refreshToken, remember);
                setUser(getAuthUserFromStoredToken());
            }
        },
        []
    );

    useEffect(() => {
        let timeout: NodeJS.Timeout;

        const doTokenRefresh = () => {
            console.log('ROBIM REFRESH TOKENU');
            const refreshToken = getToken('refreshToken');
            const decodedRefreshToken = getDecodedToken('refreshToken');
            if (
                refreshToken &&
                decodedRefreshToken &&
                decodedRefreshToken.exp
            ) {
                authApi
                    .refresh({
                        refreshToken,
                    })
                    .then((response) => {
                        updateStoredTokens(response);
                    })
                    .catch(async (err) => {
                        const processedError = await processError(err);
                        if (processedError.code === 'INVALID_CREDENTIALS') {
                            removeStoredTokens(); // refresh failed on credentials, remove all
                        }
                        enqueueSnackbar(t('auth.autoLoginFailed'), { variant: 'error', persist: true });
                    })
                    .finally(() => {
                        setLoading(false);
                    });
            } else {
                setLoading(false);
            }
        };

        const decodedToken = getDecodedToken('token');
        if (decodedToken && decodedToken.exp) {
            const timeToRefresh =
                decodedToken.exp - Math.floor(Date.now() / 1000) - 60;
            timeout = setTimeout(() => {
                doTokenRefresh();
            }, timeToRefresh * 1000);
            setLoading(false);
        } else {
            doTokenRefresh();
        }

        return () => {
            clearTimeout(timeout);
        };
    }, [user, enqueueSnackbar, t]);

    const logIn = async (credentials: LoginParams) => {
        const response = await authApi.login(credentials);
        updateStoredTokens(response, credentials.remember);
    };

    const logOut = () => {
        removeStoredTokens();
    };

    const authContext: AuthContextValue = {
        user,
        logIn,
        logOut,
    };

    console.log('Renderujem AuthContext providera');

    return (
        <AuthContext.Provider value={authContext}>
            {loading ? (
                <div style={{ width: '100%', height: '100%' }}>
                    <Loader loading={true}></Loader>
                </div>
            ) : (
                <React.Fragment>{children}</React.Fragment>
            )}
        </AuthContext.Provider>
    );
};

export default AuthProvider;