import ReactDOM from "react-dom/client";

import "./index.css";
import App from "./App";
import { CartContextProvider } from "./context/CartContext";
import { ApplicationContextProvider } from "./context/ApplicationContext";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
    <ApplicationContextProvider>
        <CartContextProvider>
            <App />
        </CartContextProvider>
    </ApplicationContextProvider>
);
