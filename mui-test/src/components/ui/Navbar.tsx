import {Mail, Notifications} from '@mui/icons-material';
import {
    AppBar,
    Avatar,
    Badge,
    InputBase,
    Menu,
    MenuItem,
    styled,
    Toolbar,
    Typography
} from '@mui/material';
import {Stack, width} from '@mui/system';
import React, {useState} from 'react'
import AvatarIcon from '../../pictures/face.png';

const StyledToolbar = styled(Toolbar)({display: "flex", justifyContent: "space-between"})

const StyledSearchField = styled(InputBase)(({theme}) => ({backgroundColor: "white", padding: "0 10px", borderRadius: theme.shape.borderRadius, width: "40%"}))

const Navbar = () => {
    const [anchorEl, setAnchorEl] = React.useState < null | HTMLElement > (null);
    const open = Boolean(anchorEl);
    const handleClick = (event : React.MouseEvent < HTMLElement >) => {
        event.preventDefault;
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };
    return (
        <AppBar position="sticky">
            <StyledToolbar>
                <Typography>
                    NAV-BAR
                </Typography>
                <StyledSearchField placeholder='search...'/>
                <Stack direction="row"
                    spacing={3}
                    alignItems="center">
                    <Stack direction="row"
                        spacing={3}
                        alignItems="center"
                        sx={
                            {
                                display: {
                                    xs: "none",
                                    sm: "block"
                                }
                            }
                    }>
                        <Badge badgeContent="2" color="error">
                            <Mail/>
                        </Badge>
                        <Notifications/>
                    </Stack>
                    <Stack direction="row"
                        spacing={1}
                        alignItems="center">
                        <Badge variant="dot" color="error"
                            sx={
                                {
                                    display: {
                                        xs: "block",
                                        sm: "none"
                                    }
                                }
                        }>
                            <Avatar src={AvatarIcon}
                                onClick={handleClick}/>
                        </Badge>
                        <Avatar src={AvatarIcon}
                            sx={
                                {
                                    display: {
                                        xs: "none",
                                        sm: "block"
                                    }
                                }
                            }
                            onClick={handleClick}/>
                        <Typography>User Name</Typography>
                    </Stack>
                </Stack>
                <Menu id="demo-positioned-menu" aria-labelledby="demo-positioned-button"
                    anchorEl={anchorEl}
                    open={open}
                    onClose={handleClose}
                    anchorOrigin={
                        {
                            vertical: 'top',
                            horizontal: 'left'
                        }
                    }
                    transformOrigin={
                        {
                            vertical: 'top',
                            horizontal: 'left'
                        }
                }>
                    <MenuItem onClick={handleClose}>Profile</MenuItem>
                    <MenuItem onClick={handleClose}>My account</MenuItem>
                    <MenuItem onClick={handleClose}>Logout</MenuItem>
                </Menu>
            </StyledToolbar>
        </AppBar>
    )
}

export default Navbar;
