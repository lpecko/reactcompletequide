// import { useState } from "react";
import { useContext } from "react";
import "./App.css";
import NewTodo from "./components/NewTodo";
import Todos from "./components/Todos";
import Todo from "./models/todo";
import TodosContext from "./store/todos-context";

function App() {
    // const [todos, setTodos] = useState([new Todo("Learn React"), new Todo("Learn Typescript")]);
    // const [todos, setTodos] = useState<Todo[]>([]);

    // const removeTodo = (id: number) => {
    //     setTodos(todos.filter((todo) => todo.id !== id));
    // };

    const ctx = useContext(TodosContext);

    return (
        <div>
            {/* <NewTodo addTodo={(todo: Todo) => setTodos((prev) => prev.concat(todo))}></NewTodo>
            <Todos items={todos} remove={removeTodo} /> */}
            <NewTodo addTodo={(todo: Todo) => ctx.addTodo(todo)}></NewTodo>
            <Todos items={ctx.items} remove={(id: number) => ctx.removeTodo(id)} />
        </div>
    );
}

export default App;
