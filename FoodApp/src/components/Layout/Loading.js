import classes from './Loading.module.css'

function Loading () {
    return (
        <div className={classes.backdrop}>
            <div className={classes['lds-ellipsis']}><div></div><div></div><div></div><div></div></div>
        </div>
    )
}

export default Loading;