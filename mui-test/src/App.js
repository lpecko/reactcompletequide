import { Box, Stack } from '@mui/material';
import React, { Component } from 'react';
import './App.css';
import Feed from './components/ui/Feed';
import Navbar from './components/ui/Navbar';
import Rightbar from './components/ui/Rightbar';
import Sidebar from './components/ui/Sidebar';

class App extends Component {
  render() {
    return (
      <Box bgcolor={"background.default"} color={"text.primary"}>
        <Navbar />
        <Stack direction="row" spacing={2} justifyContent="space-between">
          <Sidebar />
          <Rightbar />
          <Feed />
        </Stack>
      </Box>
    );
  }
}

export default App;
