import React, { useState } from "react";
import Todo from "../models/todo";

const TodosContext = React.createContext<{
    items: Todo[];
    addTodo: (todo: Todo) => void;
    removeTodo: (id: number) => void;
}>({
    items: [],
    addTodo: (todo: Todo) => {},
    removeTodo: (id: number) => {},
});

export default TodosContext;

export const TodosContextProvider: React.FC<{}> = (props) => {
    const [items, setItems] = useState<Todo[]>([]);

    const addTodoFunction = (todo: Todo) => {
        setItems((oldItems) => oldItems.concat(todo));
    };

    const removeTodoFunction = (id: number) => {
        setItems((oldItems) => oldItems.filter((todo) => todo.id !== id));
    };

    return (
        <TodosContext.Provider value={{ items: items, addTodo: addTodoFunction, removeTodo: removeTodoFunction }}>
            {props.children}
        </TodosContext.Provider>
    );
};
