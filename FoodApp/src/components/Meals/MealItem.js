import { useContext } from "react";
import CartContext from "../../context/CartContext";
import classes from "./MealItem.module.css";
import MealItemForm from "./MealItemForm";

function MealItem(props) {
    const cartCtx = useContext(CartContext);

    function onClick(amount) {
        cartCtx.addMeal({
            id: props.id,
            name: props.name,
            price: props.price,
            amount: amount,
        });
    }

    return (
        <div className={classes.meal}>
            <div>
                <h3>{props.name}</h3>
                <p className={classes.description}>{props.description}</p>
                <p className={classes.price}>{props.price}€</p>
            </div>
            <MealItemForm onClick={onClick} id={props.id} />
        </div>
    );
}

export default MealItem;
