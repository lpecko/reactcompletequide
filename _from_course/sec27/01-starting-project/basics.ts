//primitive types
let name: string;
name = "Elen";
let age: number;
age = 28;
let isWoman: boolean;
isWoman = false;
// isWoman = "adfs";

//arrays
let names: string[];
names = ["Elen", "Peter"];

// one row declaration
let ordName = "Peter"; //declared with first initialization type (type inferrence)
let ordNameRedundant: string = "Peter"; // redundant type declaration
// let ordNameWrong: string = 15;

// objects
let person: {
    name: string;
    age: number;
    isWoman: boolean;
};

person = {
    name: "Elen",
    age: 28,
    isWoman: false,
};

// object arrays
let persons: {
    name: string;
    age: number;
    isWoman: boolean;
}[];

persons = [
    { name: "Elen", age: 28, isWoman: false },
    { name: "Peter", age: 24, isWoman: false },
];

// union types
let anyType: any; // default type, like javascript
let numStringType: number | string;

anyType = 12;
anyType = "Paul";
anyType = true;

numStringType = 12;
numStringType = "Paul";
// numStringType = true; //wrong

// type aliases
type PersonType = {
    name: string;
    age: number;
    isWoman: boolean;
};
let personTyped: PersonType;
let personArrayTyped: PersonType[];

// function & types
function add(a: number, b: number) {
    return a + b;
}

function addRedundant(a: number, b: number): number {
    // return type definition is redundant
    return a + b;
}

const remove = (a: number, b: number): number => {
    return a - b;
};

//Generics
function insertAtBeginning(array: any[], value: any) {
    return [value, ...array];
}
const array = insertAtBeginning([1, 2, 3], 4); // 4,1,2,3
array[0].split(""); // its ok, because not type defined

function insertAtBeginningTyped<T>(array: T[], value: T) {
    return [value, ...array];
}
const arrayOfNumbers = insertAtBeginningTyped([1, 2, 3], 4); // 4,1,2,3
//arrayOfNumbers[0].split(""); // its clear whats type is it, number cannot be splitted;
const arrayOfStrings = insertAtBeginningTyped(["1 a", "2 a", "3 a"], "4 a");
arrayOfStrings[0].split(" ");

const arrayOfBooleans = insertAtBeginningTyped<boolean>([true, true], false);
const arrayOfBooleansWD: boolean[] = insertAtBeginningTyped([true, true], false);
// const arrayOfBooleansWD: boolean[] = insertAtBeginningTyped([true, "true"], false); // wrong



export {}; // export do ineho modulu. tu je to iba fix problemu z opakovanim nazvov premennyh v core suboroch
