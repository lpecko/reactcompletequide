import { Fragment } from 'react';
import Header from './components/Header';
import Auth from './components/Auth';
import Counter from './components/Counter';
import { useSelector } from 'react-redux';

function App() {

  const authenticated = useSelector(state => state.auth.isAuthenticated)

  return (
    <Fragment>
      <Header/>
      {!authenticated && <Auth/>}
      {authenticated && <Counter/>}
    </Fragment>
  );
}

export default App;

