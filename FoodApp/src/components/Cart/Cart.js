import classes from "./Cart.module.css";
import Modal from "../UI/Modal";
import { useContext, useEffect, useState } from "react";
import CartContext from "../../context/CartContext";
import CartList from "./CartList";
import CartForm from "./CartForm";

function Cart(props) {
    const cartCtx = useContext(CartContext);
    const [totalPrice, setTotalPrice] = useState(0);
    const [page, setPage] = useState(0);
    const [formData, setFormData] = useState(null);
    const [formDataValid, setFormDataValid] = useState(false);

    function orderHandler(event) {
        event.preventDefault();
        setPage(1);
    }

    function closeCartHandler(event) {
        event.preventDefault();
        cartCtx.setShowedCart(false);
    }

    function resetCartHandler(event) {
        event.preventDefault();
        cartCtx.resetCart();
    }

    function backCartHandler(event) {
        event.preventDefault();
        setPage(page - 1);
    }

    function sendOrderHandler(event) {
        event.preventDefault();
        let completeOrder = {
            personInfo: formData,
            order: cartCtx.mealsCart,
        };

        fetch("https://react-http-f4814-default-rtdb.europe-west1.firebasedatabase.app/ordered-meals.json", {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(completeOrder)
        });

        console.log(completeOrder);

        cartCtx.setShowedCart(false);
        console.log("Meals ordered");
        cartCtx.resetCart();
    }

    function formDataHandler(data) {
        setFormData(data);
    }

    function formValidHandler(data) {
        setFormDataValid(data);
    }

    useEffect(() => {
        setTotalPrice(cartCtx.mealsCart.map((m) => Number(m.amount) * Number(m.price)).reduce((a, b) => a + b, 0));
    }, [cartCtx.mealsCart]);

    return (
        <Modal>
            {page === 0 && <CartList mealsCart={cartCtx.mealsCart} />}
            {page === 1 && <CartForm setFormData={formDataHandler} setIsValid={formValidHandler} />}
            <div className={classes.total}>
                Total Amount
                <span>{totalPrice.toFixed(2)} €</span>
            </div>
            <div className={classes.actions}>
                <span>
                    <button onClick={closeCartHandler}>Close</button>
                    {page === 0 && <button onClick={resetCartHandler}>Reset</button>}
                    {page === 0 && (
                        <button onClick={orderHandler} disabled={cartCtx.mealsCart.length === 0}>
                            Order
                        </button>
                    )}
                    {page > 0 && <button onClick={backCartHandler}>Back</button>}
                    {page > 0 && (
                        <button onClick={sendOrderHandler} disabled={!formDataValid}>
                            SendOrder
                        </button>
                    )}
                </span>
            </div>
        </Modal>
    );
}

export default Cart;
