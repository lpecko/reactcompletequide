package sk.pecko.attedance.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AttedanceBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(AttedanceBackendApplication.class, args);
    }

}
