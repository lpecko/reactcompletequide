import React, { Fragment, useContext } from "react";
import Cart from "./components/Cart/Cart";
import Header from "./components/Layout/Header";
import Loading from "./components/Layout/Loading";
import AvailableMeals from "./components/Meals/AvailableMeals";
import ApplicationContext from "./context/ApplicationContext";
import CartContext from "./context/CartContext";

function App() {
    const cartCtx = useContext(CartContext);
    const appCtx = useContext(ApplicationContext);

    return (
        <Fragment>
            {appCtx.isLoading && <Loading />}
            <Header />
            <AvailableMeals />
            {cartCtx.showedCart && <Cart />}
        </Fragment>
    );
}

export default App;
