import { useState } from "react";
import classes from "./CartForm.module.css";

function CartForm(props) {
    const [nameTouched, setNameTouched] = useState(false);
    const [surnameTouched, setSurnameTouched] = useState(false);
    const [addressTouched, setAddressTouched] = useState(false);
    const [emailTouched, setEmailTouched] = useState(false);
    const [nameValid, setNameValid] = useState(true);
    const [surnameValid, setSurnameValid] = useState(true);
    const [addressValid, setAddressValid] = useState(true);
    const [emailValid, setEmailValid] = useState(true);

    function onChangeHandler(event) {
        const id = event.target.id;
        const value = event.target.value;
        if (id === "name") {
            setNameTouched(true);
            setNameValid(value !== "");
        } else if (id === "surname") {
            setSurnameTouched(true);
            setSurnameValid(value !== "");
        } else if (id === "address") {
            setAddressTouched(true);
            setAddressValid(value !== "");
        } else if (id === "email") {
            setEmailTouched(true);
            setEmailValid(value !== "" && value.includes('@'));
        }

        props.setIsValid(
            (nameTouched && nameValid) &&
                (surnameTouched && surnameValid) &&
                (addressTouched && addressValid) &&
                (emailTouched && emailValid)
        );

        props.setFormData({
            name: event.target.form["name"].value,
            surname: event.target.form["surname"].value,
            address: event.target.form["address"].value,
            email: event.target.form["email"].value,
        });
    }

    return (
        <form className={classes["cart-form"]}>
            <div>
                <label htmlFor='name'>Name</label>
                <input id='name' type='text' onChange={onChangeHandler} className={nameValid ? classes['cart-form'] : classes['cart-form-invalid']}></input>
            </div>
            <div>
                <label htmlFor='surname'>Surname</label>
                <input id='surname' type='text' onChange={onChangeHandler} className={surnameValid ? classes['cart-form'] : classes['cart-form-invalid']}></input>
            </div>
            <div>
                <label htmlFor='address'>Address</label>
                <input id='address' type='text' onChange={onChangeHandler} className={addressValid ? classes['cart-form'] : classes['cart-form-invalid']}></input>
            </div>
            <div>
                <label htmlFor='email'>Email address</label>
                <input id='email' type='text' onChange={onChangeHandler} className={emailValid ? classes['cart-form'] : classes['cart-form-invalid']}></input>
            </div>
        </form>
    );
}

export default CartForm;
