const basePath = "http://localhost:3000/api";

export const getTestText = async () => {
    const data = await fetch(`${basePath}/test`);
    return data.text();
};
