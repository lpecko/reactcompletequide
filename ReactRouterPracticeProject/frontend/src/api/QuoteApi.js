const basePath = window.env.baseApiUrl;

export const getQuotes = async (sort) => {
    const data = await fetch(`${basePath}/quotes${sort != null && sort !== "" ? `?sort=${sort}` : ""}`);
    const jsonData = await data.json();
    return jsonData;
};

export const saveQuotes = async (quote) => {
    await fetch(`${basePath}/new-quote`, {
        method: "POST",
        body: JSON.stringify(quote),
        headers: {
            "Content-Type": "application/json",
        },
    });
};

export const getQuote = async (id) => {
    const data = await fetch(`${basePath}/quotes/${id}`);
    if (data) {
        const jsonData = await data.json();
        return jsonData;
    }
};
