import { useContext } from 'react';
import CartContext from '../../context/CartContext';
import classes from './CartItem.module.css';

const CartItem = (props) => {

  const cartCtx = useContext(CartContext);

  const price = `${props.price.toFixed(2)} €`;

  function decMealsHandler(event) {
    console.log('decMealsHandler')
    event.preventDefault();
    cartCtx.decMealsCount(props.id)
  }

  function incMealsHandler(event) {
    console.log('incMealsHandler')
    event.preventDefault();
    cartCtx.incMealsCount(props.id)
  }

  return (
    <li className={classes['cart-item']}>
      <div>
        <h2>{props.name}</h2>
        <div className={classes.summary}>
          <span className={classes.price}>{price}</span>
          <span className={classes.amount}>x {props.amount}</span>
        </div>
      </div>
      <div className={classes.actions}>
        <button onClick={decMealsHandler}>−</button>
        <button onClick={incMealsHandler}>+</button>
      </div>
    </li>
  );
};

export default CartItem;
