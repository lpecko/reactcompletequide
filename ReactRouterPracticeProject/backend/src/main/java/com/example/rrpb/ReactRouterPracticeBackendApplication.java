package com.example.rrpb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReactRouterPracticeBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReactRouterPracticeBackendApplication.class, args);
    }

}
