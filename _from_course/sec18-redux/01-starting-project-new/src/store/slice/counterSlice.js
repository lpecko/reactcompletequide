import {createSlice} from '@reduxjs/toolkit';

const initialState = {
    counter: 0,
    showCounter: true
}

const counterSlice = createSlice({
    name: 'counter',
    initialState,
    reducers: {
        inc(state) { 
            state.counter++; 
        },
        inc5(state, action) { 
            console.log(action.payload);
            state.counter = state.counter + action.payload.amount; 
        },
        dec(state) {
            state.counter--; 
        },
        toggleCounter(state) { 
            state.showCounter = !state.showCounter; 
        },
    }
});

export const counterActions = counterSlice.actions;
export default counterSlice;