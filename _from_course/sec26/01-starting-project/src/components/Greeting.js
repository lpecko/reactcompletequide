import { useState } from "react";
import Output from "./Output";

const Greeting = () => {
    const [pText, setPText] = useState("Hello");

    const clickHandler = () => {
        setPText("This text showed after chlick button on the screen!");
    };

    return (
        <div>
            <h2>Hello world!</h2>
            <Output>{pText}</Output>
            <button onClick={clickHandler}>ChangeText</button>
        </div>
    );
};

export default Greeting;
