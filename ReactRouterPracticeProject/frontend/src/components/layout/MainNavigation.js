import { NavLink } from "react-router-dom";
import classes from "./MainNavigation.module.css";
const MainNavigation = () => {
    return (
        <header className={classes.header}>
            <div className={classes.logo}>Quotes</div>
            <nav className={classes.nav}>
                <ul>
                    <li>
                        <NavLink className={(navData) => (navData.isActive ? classes.active : "")} to='/quotes'>
                            Quotes
                        </NavLink>
                    </li>
                    <li>
                        <NavLink className={(navData) => (navData.isActive ? classes.active : "")} to='/new-quote'>
                            New quote
                        </NavLink>
                    </li>
                </ul>
            </nav>
        </header>
    );
};

export default MainNavigation;
