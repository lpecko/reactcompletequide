import React, { Fragment, useState } from "react";

const ApplicationContext = React.createContext({
    quotes: [],
    addQuote: () => {},
});

export default ApplicationContext;

export function ApplicationContextProvider(props) {
    const [quotes, setQuotes] = useState([]);

    const addQuoteToList = (quote) => {
        setQuotes(...quote, {
            id: `q${Math.random}`,
            author: quote.author,
            text: quote.text,
        });
    };

    return (
        <ApplicationContext.Provider
            value={{
                quotes: quotes,
                addQuote: addQuoteToList,
            }}
        >
            <Fragment>{props.children}</Fragment>
        </ApplicationContext.Provider>
    );
}
