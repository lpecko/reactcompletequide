import { useEffect } from "react";
import classes from "./HighlightedQuote.module.css";

const HighlightedQuote = (props) => {
    useEffect(() => {
        console.log("HighlightedQuote");
        console.log(props.text);
        console.log(props.author);
    }, [props]);
    return (
        <figure className={classes.quote}>
            <p>{props.text}</p>
            <figcaption>{props.author}</figcaption>
        </figure>
    );
};

export default HighlightedQuote;
