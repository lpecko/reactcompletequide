import { Fragment, useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";

import QuoteItem from "./QuoteItem";
import classes from "./QuoteList.module.css";

import { getQuotes } from "../../api/QuoteApi";

const QuoteList = (props) => {
    const navigate = useNavigate();
    const location = useLocation();
    const queryParams = new URLSearchParams(location.search);
    let sort = queryParams.get("sort");
    const [sortedQuotes, setSortedQuotes] = useState([]);

    useEffect(() => {
        console.log(sort);
        const getData = async () => {
            const jsonData = await getQuotes(sort);
            setSortedQuotes(jsonData);
        };
        getData();
    }, [sort]);

    const sortingHandler = () => {
        navigate(`?sort=${sort === "desc" || sort == null ? "asc" : "desc"}`);
        // history.push({
        //     pathname: location.pathname,
        //     search: `?sort=${sort === "desc" || sort == null ? "asc" : "desc"}`,
        // });
        // history.push(`${location.pathname}?sort=${isSortAsc ? "desc" : "asc"}`);
    };

    return (
        <Fragment>
            <div className={classes.sorting}>
                <button onClick={sortingHandler}>Sort {sort === "desc" ? "Descending" : "Ascending"}</button>
            </div>
            <ul className={classes.list}>
                {sortedQuotes.map((quote) => (
                    <QuoteItem key={quote.id} id={quote.id} author={quote.author} text={quote.text} />
                ))}
            </ul>
        </Fragment>
    );
};

export default QuoteList;
