import classes from "./Input.module.css";

function Input(props) {

    function changeHandler(event) {
        props.changed(event.target.value);
    }

    return (
        <div className={classes.input}>
            <label>Amount</label>
            <input type='number' min={0} value={props.value} onChange={changeHandler} />
        </div>
    );
}

export default Input;
