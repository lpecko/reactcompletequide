import { Fragment, useEffect, useState } from "react";
import { Link, Route, Routes, useParams } from "react-router-dom";
import { getQuote } from "../api/QuoteApi";
import Comments from "../components/comments/Comments";
import HighlightedQuote from "../components/quotes/HighlightedQuote";
import NoQuotesFound from "../components/quotes/NoQuotesFound";

const QuoteDetail = () => {
    const params = useParams();
    const [quote, setQuote] = useState();
    useEffect(() => {
        const getData = async () => {
            const jsonData = await getQuote(params.id);
            console.log(jsonData);
            setQuote(jsonData);
        };
        getData();
    }, [params.id]);

    if (!quote) {
        return <NoQuotesFound />;
    }
    return (
        <Fragment>
            <HighlightedQuote author={quote.author} text={quote.text} />
            {/* <Route path={`/quotes/${params.id}`} exact>
                <div className='centered'>
                    <Link className='btn--flat' to={`/quotes/${params.id}/comments`}>
                        Load comments
                    </Link>
                </div>
            </Route>
            <Route path={`/quotes/${params.id}/comments`}>
                <Comments />
            </Route> */}
            <Routes>
                <Route
                    path=''
                    element={
                        <div className='centered'>
                            <Link className='btn--flat' to={"comments"}>
                                Load comments
                            </Link>
                        </div>
                    }
                />
                <Route path={"comments"} element={<Comments />} />
            </Routes>
        </Fragment>
    );
};

export default QuoteDetail;
