import { useEffect } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import classes from "./NewEmployee.module.css";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

interface IFormInput {
    name: string;
    surname: string;
    email: string;
}

const schema = yup
    .object({
        name: yup.string().min(2, "Must contains more than 2 characters!").required(),
        surname: yup.string().min(2, "Must contains more than 2 characters!").required(),
        email: yup.string().email("Invalid email format! please check it").required(),
    })
    .required();

const NewEmployee = () => {
    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm<IFormInput>({ resolver: yupResolver(schema) });
    const submitHandler: SubmitHandler<IFormInput> = (data) => {
        console.log(data);
    };

    useEffect(() => {
        console.log(errors);
    }, [errors]);

    return (
        <form className={classes.form} onSubmit={handleSubmit(submitHandler)}>
            <div>
                <label htmlFor='name'>Name</label>
                <input id='name' type='text' {...register("name")} />
                <p>{errors.name?.message}</p>
            </div>
            <div>
                <label htmlFor='surname'>Surname</label>
                <input id='surname' type='text' {...register("surname")} />
                <p>{errors.surname?.message}</p>
            </div>
            <div>
                <label htmlFor='email'>Email</label>
                <input id='email' type='text' {...register("email")} />
                <p>{errors.email?.message}</p>
            </div>
            <button type='submit'>Send</button>
        </form>
    );
};

export default NewEmployee;
