package com.example.rrpb;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.accept.MediaTypeFileExtensionResolver;

import java.io.File;
import java.net.URLConnection;

@SpringBootTest
class ReactRouterPracticeBackendApplicationTests {

    @Test
    void contextLoads() {
        File file = new File("product.xslx");
        String mimeType = URLConnection.guessContentTypeFromName(file.getName());
        System.out.println(mimeType);
    }

}
