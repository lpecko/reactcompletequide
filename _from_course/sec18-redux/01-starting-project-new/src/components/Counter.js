import classes from './Counter.module.css';
import { useSelector, useDispatch } from 'react-redux';
import { counterActions } from '../store/slice/counterSlice';
import { useEffect } from 'react';

let isInitial = true;

const Counter = () => {

  const counter = useSelector(state => state.counter.counter);
  const showCounter = useSelector(state => state.counter.showCounter);
  const dispatch = useDispatch();
  
  useEffect(() => {
    // jedna z ciest ako vykonat asynchronnu ulohu ulozenia na server
    // fetch('https://backend.dev/saveCounter', {
    //   method: 'PUT',
    //   body: JSON.stringify(counter)
    // });
    if(isInitial) {
      isInitial = false;
      return;
    }

    const save = async () => {
      console.log('saving state ...');
      setTimeout(function() {
        console.log('saved')
      }, 500);
    }

    save().catch((error) => {
      console.error(error);
    });
  }, [counter])
  

  
  const toggleCounterHandler = () => {
    dispatch(counterActions.toggleCounter());
  };

  const incHandler = () => {
    dispatch(counterActions.inc());
  };

  const inc5Handler = () => {
    dispatch(counterActions.inc5({amount: 5}));    
  };

  const decHandler = () => {
    dispatch(counterActions.dec());
  };


  // const toggleCounterHandler = () => {
  //   dispatch({ type: 'toggle' });
  // };

  // const incHandler = () => {
  //   dispatch({ type: 'inc' });
  // };

  // const inc5Handler = () => {
  //   dispatch({ type: 'inc5', amount: 5 });
  // };

  // const decHandler = () => {
  //   dispatch({ type: 'dec' });
  // };

  return (
    <main className={classes.counter}>
      <h1>Redux Counter</h1>
      {showCounter && <div className={classes.value}>{counter}</div>}
      <div>
        <button onClick={incHandler}>Increment</button>
        <button onClick={inc5Handler}>Increase by 5</button>
        <button onClick={decHandler}>Decrement</button>
      </div>
      <button onClick={toggleCounterHandler}>Toggle Counter</button>
    </main>
  );
};

export default Counter;
