import classes from "./MealItemForm.module.css";
import Input from "../UI/Input";
import { useContext, useEffect, useState } from "react";
import CartContext from "../../context/CartContext";

function MealItemForm(props) {
    const [amount, setAmount] = useState(0);
    const cartCtx = useContext(CartContext);

    function changeHandler(amount) {
        setAmount(amount);
    }

    function clickHandler() {
        if (amount !== "" && amount > 0) {
            props.onClick(amount);
        }
    }

    useEffect(() => {
        const index = cartCtx.mealsCart.findIndex(obj => obj.id === props.id);
        if(index !== -1) {
            setAmount(cartCtx.mealsCart[index].amount);
        }
    }, [cartCtx.mealsCart, props.id]);

    return (
        <div className={classes.form}>
            <Input value={amount} changed={changeHandler} />
            <button disabled={amount === '' || amount < 1 } onClick={clickHandler}>+ Add</button>
        </div>
    );
}

export default MealItemForm;
