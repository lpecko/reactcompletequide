import "./Modal.css";
import Card from "./Card";

function Modal(props) {
    const classes = "modal " + props.className;
    return (
        <div className="backdrop">
            <Card className={classes}>{props.children}</Card>
        </div>
    );
}

export default Modal;
