import "./App.css";
import Body from "./component/layout/Body";
import Header from "./component/layout/Header";

function App() {
    return (
        <div>
            <header>
                <Header />
            </header>
            <Body />
        </div>
    );
}

export default App;
