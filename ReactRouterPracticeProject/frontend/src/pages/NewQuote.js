import QuoteForm from "../components/quotes/QuoteForm";

const NewQuote = () => {
    return <QuoteForm isLoading={false} />;
};

export default NewQuote;
