import React, { Suspense } from "react";
import { Navigate, Route, Routes } from "react-router-dom";
import Layout from "./components/layout/Layout";
import LoadingSpinner from "./components/ui/LoadingSpinner";
import AllQuotes from "./pages/AllQuotes";
// import NewQuotes from "./pages/NewQuote";
// import QuoteDetail from "./pages/QuoteDetail";

const NewQuotes = React.lazy(() => import("./pages/NewQuote")); // lazy import
const QuoteDetail = React.lazy(() => import("./pages/QuoteDetail")); // lazy import

function App() {
    return (
        <Layout>
            <Suspense fallback={<LoadingSpinner />}>
                <Routes>
                    <Route path='/' element={<Navigate to='quotes' />} />
                    <Route path='/quotes' element={<AllQuotes />} />
                    <Route path='/quotes/:id/*' element={<QuoteDetail />} />
                    <Route path='/new-quote' element={<NewQuotes />} />
                </Routes>
            </Suspense>
        </Layout>
    );
}

export default App;
