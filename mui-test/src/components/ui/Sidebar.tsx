import React from 'react'
import {Home} from '@mui/icons-material';
import {
    List,
    ListItem,
    ListItemButton,
    ListItemIcon,
    ListItemText
} from '@mui/material';
import {Box} from '@mui/system';

const Sidebar = () => {
    return (
      <Box flex={1} p={2} sx={{ display: { xs: "none", sm: "block" } }}>
        <Box position="fixed" >
            <List>
                <ListItem disablePadding>
                    <ListItemButton component="a" href='#home'>
                      <ListItemIcon>
                        <Home />
                      </ListItemIcon>
                        <ListItemText primary="Homepage"/>
                    </ListItemButton>
                </ListItem>
            </List>
        </Box>
     </Box>
    )
}

export default Sidebar;
