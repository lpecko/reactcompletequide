import React, { useState } from "react";

const CartContext = React.createContext({
    showedCart: false,
    mealsCart: [],
    addMeal: (mealToCard) => {},
    setShowedCart: (show) => {},
    resetCart: () => {},
    incMealsCount: (id) => {},
    decMealsCount: (id) => {},
});

export default CartContext;

export function CartContextProvider(props) {
    //use map
    const [mealsCart, setMealsCart] = useState([]);
    const [showedCart, setShowedCartVal] = useState(false);

    function addMealHandler(mealToCard) {
        const newItem = {
            id: mealToCard.id,
            name: mealToCard.name,
            price: Number(mealToCard.price),
            amount: Number(mealToCard.amount),
        };

        const index = mealsCart.findIndex((obj) => obj.id === mealToCard.id);
        if (index !== -1) {
            let newMealsCart = [...mealsCart];
            newMealsCart[index] = newItem;
            setMealsCart(newMealsCart);
        } else {
            setMealsCart([...mealsCart, newItem]);
        }
    }

    function setShowedCart(show) {
        setShowedCartVal(show);
    }

    function resetCart() {
        setMealsCart([]);
    }

    function incMealsCount(id) {
        let newMealsCart = [...mealsCart];
        const indexToChange = newMealsCart.findIndex((obj) => obj.id === id);
        newMealsCart[indexToChange].amount = newMealsCart[indexToChange].amount + 1;
        setMealsCart(newMealsCart);
    }

    function decMealsCount(id) {
        let newMealsCart = [...mealsCart];
        const indexToChange = newMealsCart.findIndex((obj) => obj.id === id);
        if (newMealsCart[indexToChange].amount > 1) {
            newMealsCart[indexToChange].amount = newMealsCart[indexToChange].amount - 1;
        } else {
            newMealsCart.splice(indexToChange);
        }
        setMealsCart(newMealsCart);
    }

    return (
        <CartContext.Provider
            value={{
                showedCart: showedCart,
                mealsCart: mealsCart,
                addMeal: addMealHandler,
                setShowedCart: setShowedCart,
                resetCart: resetCart,
                incMealsCount: incMealsCount,
                decMealsCount: decMealsCount,
            }}
        >
            {props.children}
        </CartContext.Provider>
    );
}
