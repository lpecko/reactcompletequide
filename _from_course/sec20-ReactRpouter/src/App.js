import { Fragment } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import Products from "./pages/Products";
import Welcome from "./pages/Welcome";
import MainHeader from "./components/MainHeader";
import ProductDetail from "./pages/ProductDetail";

function App() {
    return (
        <Fragment>
            <MainHeader />
            <main>
                {/* default -> nothing -> natiahne vsetko co splna podmienku aj ciastocne tzn. aj /products aj /products/:productId */}
                <Switch>
                    {/*  -> natiahne prvy, ktory splna podmienku tzn vzy iba /products */}
                    <Route path='/'>
                        <Redirect to='/welcome' />
                    </Route>
                    <Route path={"/welcome"}>
                        <Welcome />
                    </Route>
                    {/*exact zabezpeci, ze sa /products natiahne iba pri presnej zhode*/}
                    <Route path={"/products"} exact>
                        <Products />
                    </Route>
                    <Route path={"/products/:productId"}>
                        <ProductDetail />
                    </Route>
                </Switch>
            </main>
        </Fragment>
    );
}

export default App;
