import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import Greeting from "./Greeting";

describe("Greeting tests", () => {
    test("HelloWorldTest", () => {
        //Arrange
        render(<Greeting />);

        //Act

        //Assert
        const helloWorlEl = screen.getByText("Hello world!");
        expect(helloWorlEl).toBeInTheDocument();
    });

    test("TestWithoutUserInteraction", () => {
        render(<Greeting />);
        const el = screen.getByText("Hello");
        expect(el).toBeInTheDocument();
    });

    test("TestUserInteraction", () => {
        render(<Greeting />);

        const button = screen.getByRole("button");
        userEvent.click(button);

        const el = screen.getByText("This text", { exact: false });
        expect(el).toBeInTheDocument();
    });

    test("TestMissingElement", () => {
        render(<Greeting />);

        const button = screen.getByRole("button");
        userEvent.click(button);

        const el = screen.queryByText("Hello", { exact: true });
        expect(el).not.toBeInTheDocument();
    });
});
