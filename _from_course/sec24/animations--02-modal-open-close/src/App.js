import React, { Component } from "react";

import "./App.css";
import Modal from "./components/Modal/Modal";
import Backdrop from "./components/Backdrop/Backdrop";
import List from "./components/List/List";
// import { CSSTransition, Transition } from "react-transition-group";

class App extends Component {
    state = {
        modalIsOpen: false,
    };

    showModal = () => {
        this.setState({ modalIsOpen: true });
    };

    closeModal = () => {
        this.setState({ modalIsOpen: false });
    };

    render() {
        return (
            <div className='App'>
                <h1>React Animations</h1>
                {/* <Transition
                    in={this.state.modalIsOpen}
                    timeout={400}
                    mountOnEnter
                    unmountOnExit
                    onEnter={console.log("on enter")}
                    onEntering={console.log("on entering")}
                    onEntered={console.log("on entered")}
                    onExit={console.log("on Exit")}
                    onExiting={console.log("on Exiting")}
                    onExited={console.log("on Exited")}
                >
                    {(state) => <Modal show={state} closed={this.closeModal} />}
                </Transition> */}

                <Modal show={this.state.modalIsOpen} closed={this.closeModal} />
                <Backdrop show={this.state.modalIsOpen} />
                <button className='Button' onClick={this.showModal}>
                    Open Modal
                </button>
                <h3>Animating Lists</h3>
                <List />
            </div>
        );
    }
}

export default App;
