import { useEffect, useState } from "react";

function useCounter(direction = "forward") {
    const [counter, setCounter] = useState(0);
    const addition = direction === "forward" ? 1 : -1;

    useEffect(() => {
        const interval = setInterval(() => {
            setCounter((prevCounter) => prevCounter + addition);
        }, 1000);

        return () => clearInterval(interval);
    }, [addition]);

    return counter;
}

export default useCounter;
