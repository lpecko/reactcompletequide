package com.example.rrpb;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
public class QuotesController {

    private List<Quote> quotes = new ArrayList<>(Arrays.asList(
            new Quote("q1", "Peter", "Text123456"),
            new Quote("q2", "Max", "Text654321"),
            new Quote("q3", "Morgan", "Text321654")));


    @GetMapping(value = "/quotes",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Quote> getQuotes(@PathParam("sort") String sort) {
        if (sort == null) {
            return quotes;
        } else {
            return quotes.stream()
                    .sorted((o1, o2) -> {
                        if (sort.equals("asc")) {
                            return o1.getId().compareTo(o2.getId());
                        } else {
                            return o2.getId().compareTo(o1.getId());
                        }
                    }).collect(Collectors.toList());
        }
    }

    @GetMapping(value = "/quotes/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Quote getQuote(@PathVariable String id) {
        Optional<Quote> quote = quotes.stream().filter(q -> q.getId().equals(id)).findAny();
        return quote.isPresent() ? quote.get() : null;
    }

    @PostMapping(value = "/new-quote",
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public void saveQuote(@RequestBody Quote q) {
        System.out.println(q);
        quotes.add(q);
    }

}
