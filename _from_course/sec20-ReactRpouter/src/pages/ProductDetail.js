import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

const ProductDetail = () => {
    const params = useParams();
    const [text, setText] = useState("");

    useEffect(() => {
        console.log("Loading Product Detail");
        if (params.productId === "p1") {
            setText("Book detail");
        } else if (params.productId === "p2") {
            setText("Carpet detail");
        } else if (params.productId === "p3") {
            setText("Course detail");
        }
    }, [setText, params]);

    return <p>{text}</p>;
};

export default ProductDetail;
