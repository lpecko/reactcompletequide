import classes from "./Header.module.css";

const Header = () => {
    return (
        <div className={classes.header}>
            <h1>Attendance</h1>
            <ul>
                <li>Add employee</li>
                <li>List of employees</li>
                <li>Attendance</li>
            </ul>
        </div>
    );
};

export default Header;
